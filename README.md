# webrtc-proxy


> Share websites without a server, using WebRTC directly from your browser

**[Click here to try the hosted verison!](https://proxy.joshi.monster)**

![Simple Demo - Screenshot of Firefox in server mode and Chrome, connected to a client. In a separate, larger Chrome window, the user visited proxy.joshi.monster/blog/serverless-htmx, but it shows the page from joshi.monster instead.](screenshots/demo-simple.png)

_Make sure you look at the address bar - read the explanation below to learn what is going on here!_

---

Ever wanted to share a local project with a collegue or you parents?

Usually, this required you to either spend the next hour setting up hosting yourself (or using a service like Netlify), or using a sketchy, poorly working port-forwarding/tunnel service that routes all your traffic through their servers.

This project takes a different approach: By abusing modern web technologies like [service workers](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers) and [WebRTC](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API), it can establish a direct peer-to-peer between browsers or our CLI tool. All requests to [proxy.joshi.monster](https://proxy.joshi.monster) (our hosted version of this app) will then be routed through the WebRTC connection directly to your peer, which will then make the "real" HTTP request on your behalf. While connected, [proxy.joshi.monster](https://proxy.joshi.monster) will act as an https-encrypted proxy mirror of your site.

Doing this, all traffic is securily sent directly between the 2 browsers - there is no server in the middle that can read your requests, at all! It only requires a so-called "signaling channel" to exchange metadata and make sure the connection can be established. We use [Pusher](pusher.com/) for this.

## The CLI server

This project also includes a CLI server that you can use instead of the website.

The client can still connect to you by just opening a special link. Only the server-side has to install the CLI.

**[Download the latest version of the CLI!](https://gitlab.com/arkandos/webrtc-proxy/-/releases)**

### Usage

```bash
# Show command line options
$ ./webrtc-proxy -h
# proxy `vite preview`
$ ./webrtc-proxy http://localhost:4173
Proxy started, go to https://proxy.joshi.monster/_proxy/xxxx-xxx-xxx-xxx-xxxxxx
```

Because of browser limitations, using the CLI has a few advantages:

### No need to worry about CORS

When you enable the proxy, the browser will eventually do [fetch requests](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Basic_concepts) to actually load the original website you're proxying. These requests will happen in the context of the domain [proxy.joshi.monster](https://proxy.joshi.monster), and need to follow [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) rules. This means that most headers can't be set, and most POST and PUT requests will fail due to additional requirements on their content-type.

Using the CLI, the browsers limitations do not apply (since there is no browser on the server-side), so requests can have any headers and bodies they like.

### Cookies and CSRF checks likely work

Another limitation of browsers are the so-called [forbidden header names](https://developer.mozilla.org/en-US/docs/Glossary/Forbidden_header_name). This prevents us from setting and reading certain headers, like `Origin` and `Referer` that is used during CSRF checks, but also headers used for cookies.

The CLI tries to emulate the behaviour of the browser as best as it can while proxying requests - this means it will add and fix those headers for you, and store and send cookies internally for every connected client.

## Limitations

Most limitations of the fully-in-browser implementation are lifted by using the CLI version instead. However, some things will likely never work:

### HMR does not work

HMR (hot module replacement) is built on WebSockets. Unfortunately, there is currently no way to intercept WebSocket connections from service workers, so those connections will just fail, and HMR will not work.

When using vite, I recommend proxying the _preview_ version instead:

```bash
npx vite build && npx vite preview
```

### Projects with custom service workers might not work

This projects makes heavy use of service workers to intercept every request and forward them to a peer. When you add your own service worker into the mix, who knows what is happening! It will probably just not work anymore.
