package main

import (
	"io"

	"github.com/pion/webrtc/v4"
)

type DataChannelReader struct {
	Msgs <-chan webrtc.DataChannelMessage
	cur  []byte
	eof  bool
}

func (r *DataChannelReader) Read(p []byte) (int, error) {
	if r.eof {
		return 0, io.EOF
	}

	if len(p) == 0 {
		return 0, nil
	}

	if len(r.cur) == 0 {
		msg, ok := <-r.Msgs

		if !ok || len(msg.Data) == 0 {
			r.eof = true
			return 0, io.EOF
		}

		r.cur = msg.Data
	}

	n := copy(p, r.cur)

	r.cur = r.cur[n:]
	return n, nil
}

type DataChannelWriter struct {
	*webrtc.DataChannel
}

func (w *DataChannelWriter) Write(p []byte) (int, error) {
	size := 16 * 1024 // pion says this is the max size for Send()
	for offs := 0; offs < len(p); offs += size {
		end := offs + size
		if end > len(p) {
			end = len(p)
		}

		chunk := p[offs:end]
		if err := w.Send(chunk); err != nil {
			return offs, err
		}
	}
	return len(p), nil
}

func (w *DataChannelWriter) Close() error {
	return w.Send([]byte{})
}
