{
    outputs = { self, nixpkgs }:
        let
            lib = nixpkgs.lib;
            systems = ["x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin"];
            forEachSystem = systems: f: lib.genAttrs systems (system: f system);
            forAllSystems = forEachSystem systems;
        in
        {
            defaultPackage = forAllSystems (system:
                with import nixpkgs { inherit system; };
                pkgs.buildGoModule {
                    pname = "webrtc-proxy";
                    version = "1.0.0";
                    src = ./cli;

                    # vendorSha256 = pkgs.lib.fakeSha256;
                    vendorSha256 = "sha256-pak7IsnMVpXaMs85fUmGxOLp+4DC6D1SJcWevYIIZHI=";

                    meta = with lib; {
                        platforms = systems;
                        description = "Share a website using WebRTC";
                        homepage = "https://gitlab.com/arkandos/webrtc-proxy";
                        license = licenses.bsd3;
                    };
                }
            );

            devShell = forAllSystems (system:
                with import nixpkgs { inherit system; };
                pkgs.mkShell {
                    buildInputs = with pkgs; [
                        go
                        gopls
                        gotools
                        go-tools
                        nodejs
                    ];
                }
            );
        };
}
