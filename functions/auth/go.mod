module joshi.monster/webrtc-proxy/auth

go 1.20

require (
	github.com/aws/aws-lambda-go v1.41.0
	github.com/pusher/pusher-http-go v4.0.1+incompatible
)

require (
	golang.org/x/crypto v0.13.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
)
