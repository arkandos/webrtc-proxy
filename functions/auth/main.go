package main

import (
	"encoding/base64"
	"os"
	"net/url"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	pusher "github.com/pusher/pusher-http-go"
)

var (
	pusherAppId = os.Getenv("PUSHER_APPID")
	pusherKey = os.Getenv("PUSHER_KEY")
	pusherSecret = os.Getenv("PUSHER_SECRET")
	pusherCluster = os.Getenv("PUSHER_CLUSTER")
	encryptionKeySalt = os.Getenv("ENCRYPTION_KEY")
)

func handler(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {

	body, err := url.ParseQuery(request.Body)
	if err != nil {
		return nil, err
	}

	channelName := body.Get("channel_name")

	encryptionKey := channelName + encryptionKeySalt
	if len(encryptionKey) > 32 {
		encryptionKey = encryptionKey[0:32]
	}

	encryptionKeyBase64 := base64.StdEncoding.EncodeToString([]byte(encryptionKey))

	client := pusher.Client{
		AppID:   pusherAppId,
		Key:     pusherKey,
		Secret:  pusherSecret,
		Cluster: pusherCluster,
		Secure:  true,
		EncryptionMasterKeyBase64: encryptionKeyBase64,
	}


	response, err := client.AuthenticatePrivateChannel([]byte(request.Body))
	if err != nil {
		return nil, err
	}

	return &events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
		Body: string(response),
	}, nil
}

func main() {
	lambda.Start(handler)
}
