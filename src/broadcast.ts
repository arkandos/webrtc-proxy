/// <reference lib="webworker" />

declare const self: ServiceWorkerGlobalScope

import pDestruct, { type DestructuredPromise } from '@jreusch/p-destruct'


export interface RequestData {
    url: string,
    method: string,
    headers: Record<string, string>,
    body: ReadableStream | null
}

export interface ResponseData {
    status: number,
    headers: Record<string, string>
    body: ReadableStream
}


/**
 * Messages sent from the service worker to the client
 */
export type Msg =
    | { tag: 'listen', server: ServerId, url: string }
    | { tag: 'inquire', server: ServerId }
    | { tag: 'connect', server: ServerId }
    | { tag: 'request', request: RequestData }
    | { tag: 'disconnect' }

export type ServerId = string & { __brand: 'server-id' }
export type ClientId = string & { __brand: 'server-id' }

/**
 * Messages are wrapped in an envelope, giving them a unique ID and an option
 * to reploy to a recieved message.
 */
interface Envelope<T> {
    msg: T
    id: MessageId
    res?: MessageId
    err?: unknown
}

type MessageId = number & { __brand: 'message-id' }

export async function getClient(addr: string): Promise<Client> {
    const client = await self.clients.get(addr)
    if (!client) {
        throw new Error('invalid address: ' + addr)
    }
    if (client.type !== 'window') {
        throw new Error(`invalid client type for ${addr}: ${client.type}`)
    }

    return client
}


export function newClientId(): ClientId {
    return crypto.randomUUID() as ClientId
}

export function newServerId(): ServerId {
    return toServerId(crypto.randomUUID())
}

export function toServerId(str: string): ServerId {
    str = str.trim()
    if (!str.length) {
        throw new Error('empty server id')
    }

    return str as ServerId
}


let lastMessageId = 0
function newMessageId(): MessageId {
    const id = ++lastMessageId
    if (lastMessageId === Number.MAX_SAFE_INTEGER) {
        lastMessageId = 0
    }

    return id as MessageId
}

function wrapMsg<T>(msg: T, inResponseTo?: MessageId): Envelope<T> {
    const id = newMessageId()
    return { msg, id, res: inResponseTo }
}

function wrapErr(err: unknown, inResponseTo: MessageId): Envelope<void> {
    const id = newMessageId()
    return { msg: undefined, id, res: inResponseTo, err }
}

function collectTransferables(obj: unknown): Transferable[] {
    if (typeof obj !== 'object' || obj == null) {
        return []
    }

    if (Array.isArray(obj)) {
        return obj.flatMap(collectTransferables)
    } else if (isTransferable(obj)) {
        return [obj]
    } else {
        return Object.values(obj).flatMap(collectTransferables)
    }
}

function isTransferable(obj: unknown): obj is Transferable {
    if (typeof obj !== 'object' || obj == null) {
        return false
    }

    const transferables = 'ArrayBuffer MessagePort ReadableStream WritableStream TransformStream WebTransportReceiveStream AudioData ImageBitmap VideoFrame OffscreenCanvas RTCDataChannel'

    return transferables.includes(obj.constructor.name)
}

const pendingResponses = new Map<MessageId, DestructuredPromise<unknown>>()
function send<T>(client: Client, msg: unknown): Promise<T> {
    const deconstructed = pDestruct<T>()
    const wrapped = wrapMsg(msg)

    pendingResponses.set(wrapped.id, deconstructed as DestructuredPromise<unknown>)

    client.postMessage(wrapped, collectTransferables(wrapped.msg))

    return deconstructed.promise
}

function sendToClient<T>(client: Client, msg: Msg): Promise<T> {
    return send<T>(client, msg)
}

export function onClientMessage(callback: (msg: Msg) => Promise<unknown>): ((e: MessageEvent<any>) => any) {
    return async function onclientMessage(e: MessageEvent<any>) {
        if (!(e.source instanceof ServiceWorker)) {
            return console.warn('Got a message from someone else but the service worker: ', e.source, e.data)
        }

        const from: ServiceWorker = e.source
        const msg = e.data as Envelope<Msg>

        try {
            const result = await callback(msg.msg)
            const wrapped = wrapMsg(result, msg.id)
            from.postMessage(wrapped, collectTransferables(result))
        } catch(err) {
            const wrapped = wrapErr(err, msg.id)
            from.postMessage(wrapped)
        }
    }
}

export async function onServiceWorkerMesssage(e: ExtendableMessageEvent) {
    if (!(e.source instanceof Client)) {
        return console.error('Got a message from something else other than a client: ', e.source, e.data)
    }

    const msg = e.data as Envelope<unknown>
    if (!msg.res) {
        return console.warn('ORPHAN RESPONSE:', msg)
    }

    const handler = pendingResponses.get(msg.res)
    if (!handler) {
        return console.warn('ORPHAN RESPONSE:', msg)
    }

    pendingResponses.delete(msg.res)
    if (msg.err) {
        handler.reject(msg.err)
    } else {
        handler.resolve(msg.msg)
    }
}

export function listen(to: Client, server: ServerId, url: string): Promise<void> {
    return sendToClient(to, { tag: 'listen', server, url })
}

export function inquire(to: Client, server: ServerId): Promise<{ url: string } | null> {
    return sendToClient(to, { tag: 'inquire', server })
}

export function connect(to: Client, server: ServerId): Promise<{ url: string } | null> {
    return sendToClient(to, { tag: 'connect', server })
}

export function request(to: Client, request: RequestData): Promise<ResponseData> {
    return sendToClient(to, { tag: 'request', request })
}

export function disconnect(to: Client): Promise<void> {
    return sendToClient(to, { tag: 'disconnect' })
}
