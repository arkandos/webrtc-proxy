import { VNode, ComponentChildren } from "preact"
import classNames from "classnames"

export default function Button(props: { type: 'button' | 'submit', onClick?: (this: HTMLButtonElement, ev: Event) => void, children: ComponentChildren }): VNode {

    return <button
        type={props.type}
        className={classNames([
            props.type === 'submit'
                ? 'hx:pointer-events-none'
                : void 0,
            'bg-white text-black px-10 py-2 font-semibold text-lg text-center border-2 border-white ring-purple-800 hover:border-purple-600 hover:bg-purple-600 hover:text-white focus:outline-none focus:border-purple-800 focus:ring'
        ])}
        hx-on:click={props.onClick
            ? `(${props.onClick?.toString()}).call(this, event)`
            : void 0}
    >

        {props.children}

        {props.type === 'submit'
            ? <span className="none hx:inline-block hx:ml-4 w-4 h-4 rounded-full hx:border-4 border-r-transparent border-current animate-spin" />
            : void 0
        }
    </button>
}
