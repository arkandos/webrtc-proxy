import { VNode } from "preact"
import classNames from "classnames"

interface Props {
    name: string
    label: string
    className?: string
    type?: string
    placeholder?: string
    value?: string
    required?: boolean
    readonly?: boolean
}

export default function Input(props: Props): VNode {
    return <label className={classNames('block', props.className)}>
        <span class="font-semibold text-lg">{props.label}</span>
        <input
            className="hx:pointer-events-none block px-10 py-3 mt-2 border-2 w-full bg-transparent placeholder-gray-400 ring-purple-800 border-white focus:border-purple-800 focus:outline-none focus:ring"
            name={props.name}
            type={props.type}
            placeholder={props.placeholder}
            value={props.value}
            required={props.required}
            readOnly={props.readonly}
        />
    </label>
}
