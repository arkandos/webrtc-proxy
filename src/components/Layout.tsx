import classNames from "classnames";
import { ComponentChildren, VNode } from "preact";

import Gitlab from "@/components/Gitlab";

import clientJsUrl from '@/client.ts?worker&url'

export default function Layout(props: { title: string, className?: string; children: ComponentChildren }): VNode {
    return <html lang="en">
        <head>
            <meta charSet="utf-8" />
            <title>WebRTC Proxy</title>
            <meta name="viewport" content="width=device-width,initial-scale=1" />
            <meta name="description" value="Proxy a website, using WebRTC" />
            <script src={clientJsUrl} defer></script>
        </head>
        <body className="p-4 text-white bg-gray-950 font-mono leading-normal">
            <a
                href="https://gitlab.com/arkandos/webrtc-proxy"
                target="_blank"
                rel="noreferrer nofollow"
                title="Fork this project on GitLab"
                className="block absolute top-0 right-0"
            >
                <Gitlab className="w-24 drop-shadow" />
            </a>

            <main className={classNames("bg-black border-white border-0 mx-auto max-w-screen-md p-4 md:border-4 md:mt-16 md:p-16", props.className)}>
                <h1 className="text-3xl font-bold text-purple-800 mb-8 md:mb-16">
                    {props.title}
                </h1>

                {props.children}
            </main>
            <footer class="mt-8 mb-4 mx-auto max-w-screen-md border-transparent px-4 md:px-0 md:border-4 md:flex justify-between text-xs">
                <a
                    href="https://joshi.monster/impressum/"
                    className="ml-auto font-semibold hover:underline"
                >
                    Impressum
                </a>
            </footer>
        </body>
    </html>
}
