import { VNode } from "preact";

import { ServerId, connect, disconnect, getClient, inquire } from "@/broadcast";
import { deleteLastConnectedClient, getLastConnectedClient, setLastConnectedClient } from "@/db";
import { error } from "@/http-error";

import Layout from "@/components/Layout";
import Input from "@/components/Input";
import Button from "@/components/Button";


interface Params {
    id: ServerId
}

export async function get(params: Params, e: FetchEvent): Promise<VNode> {
    const endpoint = `/_proxy/${params.id}`

    const isHtmx = e.request.headers.has('HX-Request')
    if (!isHtmx) {
        return <Layout title="Proxy a website, using WebRTC">
            <div hx-get={endpoint} hx-trigger="load" hx-swap="outerHTML">
                <p>Loading...</p>
            </div>
        </Layout>
    } else {
        const client = await getClient(e.clientId)
        const meta = await inquire(client, params.id)
        if(!meta) {
            // timeout, not found
            return <NotFound id={params.id} />
        }

        return <ConnectForm id={params.id} url={meta.url} />
    }
}


export async function post(params: Params, e: FetchEvent): Promise<VNode> {
    const endpoint = `/_proxy/${params.id}`

    const client = await getClient(e.clientId)
    const meta = await connect(client, params.id)
    if (!meta) {
        return <NotFound id={params.id} />
    }

    await setLastConnectedClient(client)

    return <form
        class="my-16"
        hx-delete={endpoint}
        hx-swap="outerHTML"
    >
        <p class="font-semibold text-lg">
            <span class="inline-block w-4 h-4 align-middle rounded-full bg-green-600 mr-6" />
            Connected to peer.
        </p>

        <p class="mt-6">
            Please leave this tab open for the connection to work.
        </p>
        <p class="mt-4">
            All requests to <span class="text-purple-800">{location.host}</span> in other tabs will be forwarded to the peer.
        </p>

        <Input
            name="url"
            type="url"
            className="mt-4"
            label="Requests are forwarded to this url:"
            value={meta.url}
            readonly
        />

        <div class="mt-4 flex gap-4">
            <Button
                type="button"
                onClick={function () {
                    window.open(location.origin, '_blank')
                }}
            >
                Open in new tab
            </Button>

            <Button type="submit">
                Disconnect
            </Button>
        </div>
    </form>
}

export async function delet(params: Params, e: FetchEvent): Promise<VNode> {
    const data = await e.request.formData()
    const url = data.get('url')

    if (!url || typeof url !== 'string') {
        error(400)
    }

    const client = await getLastConnectedClient()
    if (client) {
        await deleteLastConnectedClient()
        await disconnect(client)
    }

    return <ConnectForm id={params.id} url={url} />
}

export async function unload(_params: unknown, _e: FetchEvent): Promise<Response> {
    await deleteLastConnectedClient()
    // we assume the browser already disconnected the WebRTC stuff

    return new Response('Disconnected.')
}

function ConnectForm(props: { id: ServerId, url: string}): VNode {
    const endpoint = `/_proxy/${props.id}`

    return <form
        class="my-16"
        hx-post={endpoint}
        hx-swap="outerHTML"
    >
        <p>
            Your peer wants you to connect to their browser, forwarding  requests to <span class="text-purple-800">{location.host}</span> to another website reachable from their computer instead.
        </p>

        <p class="mt-4">
            Note that while it may look like you're accessing <span class="text-purple-800">{location.host}</span> in your browser, this page is not responsible for any content on this domain while the proxy is active.
        </p>

        <p class="mt-4">
            You can close this tab if you don't consent.
        </p>

        <Input
            name="url"
            type="url"
            className="mt-4"
            label={`Forward ${location.host} to this url?`}
            value={props.url}
            readonly
        />

        <div class="mt-4">
            <Button type="submit">
                Connect to peer
            </Button>
        </div>
    </form>
}

function NotFound(props: { id: ServerId }): VNode {
    const endpoint = `/_proxy/${props.id}`
    return <form
        hx-get={endpoint}
        hx-swap="outerHTML"
    >
        <p class="text-red-600 text-lg text-semibold">
            We could not connect to the your peer.
        </p>

        <p class="mt-4">
            Maybe try to contact them to make sure their tab or server is still runnning and active. Also make sure you copied the correct url.
        </p>

        <div class="mt-8">
            <Button type="submit">
                Try again
            </Button>
        </div>
    </form>
}
