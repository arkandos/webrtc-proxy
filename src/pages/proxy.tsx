/// <reference lib="webworker" />

declare const self: ServiceWorkerGlobalScope

import { render } from "preact-render-to-string"
import { all, compile, get } from "@jreusch/router-node"

import { getLastConnectedClient } from "@/db"
import { getClient, request } from "@/broadcast"

import Layout from "@/components/Layout"
import Link from "@/components/Link"

export default async function proxy(_params: unknown, e: FetchEvent): Promise<Response> {
    const req = e.request
    const url = new URL(req.url)

    const connectedClient = await getLastConnectedClient()
    if (!connectedClient) {
        const fallback = fallbackRouter(req)
        if (!fallback) {
            // should never happen
            return new Response('Not found', { status: 404 })
        }

        return fallback
    }

    let body : ReadableStream | null = null
    if (req.body === undefined && req.method !== 'GET' && req.method !== 'HEAD') {
        // request.body is not implemented in Firefox
        const blob = await req.blob()
        body = blob.stream()
    } else {
        body = req.body
    }

    const headers = Object.fromEntries(req.headers)

    try {
        const requestingClient = await getClient(e.clientId)
        // referer SHOULD get rewritten by the server to actually match the proxied
        // URL - we don't do that ourselfes to make sure we always match the server
        // implementation, regardless of what it does.
        const referer = new URL(requestingClient.url)
        headers['referer'] = referer.toString()
    } catch {
        // ignore.
        // we either don't have a client yet (so no referer)
        // or we couldn't parse the value (so just ignore it)
    }

    headers['user-agent'] = self.navigator.userAgent
    // host, origin, etc. should be added by the implementation.

    const reqData = {
        url: url.pathname + url.search,
        method: req.method,
        headers: headers,
        body: body
    }

    try {
        const resData = await request(connectedClient, reqData)
        return new Response(resData.body, resData)
    } catch(err) {
        return new Response(`${err}`, { status: 500 })
    }
}

const fallbackRouter = compile(
    req => req.method,
    req => new URL(req.url).pathname,

    // if we use the fallback, redirect to _proxy
    get('/', (_params, _req) => {
        return new Response(null, {
            status: 302,
            statusText: 'Found',
            headers: {
                'location': '/_proxy'
            }
        })
    }),

    // fallback of the fallback: return a 404 page.
    all('*', (_params, _req) => {
        const vdom = <Layout title="You're staring into the void">
            <p>
                In the distance, you believe to make out 2 options:
            </p>
            <ol class="ml-8 mt-6 list-decimal">
                <li class="mt-2">
                    <strong class="font-lg">You expected this URL to be proxied:</strong><br />
                    It looks like the tab for the connection is no longer open.
                    Try to connect anew!
                </li>
                <li class="mt-2">
                    <strong class="font-lg">You wanted to be free, but emptiness fills your heart:</strong><br />
                    There is nothing else. <Link href="/_proxy">Go back instead</Link>.
                </li>
            </ol>
        </Layout>

        const html = '<!DOCTYPE html>\n' + render(vdom)

        return new Response(html, {
            status: 404,
            headers: {
                'Content-Type': 'text/html'
            }
        })
    })
)
