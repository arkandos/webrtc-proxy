import QR from 'qrcode'

export default async function handleQr(_params: unknown, e: FetchEvent): Promise<Response> {
    const requestUrl = new URL(e.request.url)
    const href = requestUrl.searchParams.get('url')
    if (!href) {
        return new Response('url param missing', { status: 400 })
    }

    const isAbsolute = /^https?:\/\//.test(href)
    const absoluteUrl = isAbsolute ? href : `${location.origin}/${href.replace(/^\//, '')}`

    const data = await QR.toString(absoluteUrl, {
        type: 'svg',
        margin: 1,
    })

    return new Response(data, {
        headers : {
            'Content-Type': 'image/svg+xml'
        }
    })
}
