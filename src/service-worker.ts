/// <reference lib="webworker" />
import { VNode } from 'preact'
import { render } from 'preact-render-to-string'
import * as router from '@jreusch/router-node'

import { HttpError } from '@/http-error'
import { onServiceWorkerMesssage } from '@/broadcast'
import * as pages from '@/pages'

declare const self: ServiceWorkerGlobalScope

// makes typescript shut up
export type {}

// these 2 event listeners make sure the browser replaces the server worker
// as soon as possible if it detects an update.
self.addEventListener('install', event => {
    event.waitUntil(self.skipWaiting())
})

self.addEventListener('activate', event => {
    event.waitUntil(pages.cache.onActivate())
})


const uiRoutes = router.wrap(
    (next: (params: unknown, req: FetchEvent) => Promise<VNode> | null, params, req) => {
        const vdomP = next(params, req)
        if (!vdomP) {
            return null
        }

        return vdomP
            .then(async vdom => {
                const html = '<!DOCTYPE html>\n' + render(vdom)
                return new Response(html, {
                    headers: {
                        'Content-Type': 'text/html'
                    }
                })
            })
            .catch((e: unknown) => {
                if (e instanceof HttpError) {
                    return e.toResponse()
                } else if (e instanceof Error) {
                    return new Response(e.message, { status: 500 })
                } else {
                    return new Response(JSON.stringify(e), { status: 500 })
                }
            })
    },

    router.group('/',
        router.get('', pages.index.get),
        router.post('', pages.index.post),
        router.delet('', pages.index.delet)),

    router.group('/:id',
        router.get('', pages.connect.get),
        router.post('', pages.connect.post),
        router.delet('', pages.connect.delet))
)

const route = router.compile(
    (e: FetchEvent) => e.request.method,
    (e: FetchEvent) => new URL(e.request.url).pathname,

    // internal stuff here. the _proxy prefix is reserved and makes it possible
    // to always access the "backend" even if the proxy is active
    router.group('/_proxy',
        // auth for pusher
        router.all('auth', pages.handlePusherAuth),

        // disconnect
        router.post('unload', pages.connect.unload),

        // generate QR codes
        router.get('qr', pages.handleQr),

        // cache
        router.get('/assets/*', pages.cache.get),

        // Default to UI routes - here we handle everything else as a "connect" request
        uiRoutes,
    ),

    // try to proxy all other requests
    router.all('*', pages.proxy)
)

self.addEventListener('fetch', e => {
    const url = new URL(e.request.url)
    // if you request a 3rd-party page, always do the default
    if (url.hostname !== location.hostname) {
        return
    }

    e.respondWith((async () => {
        const routerResponse = await route(e)
        if (!routerResponse) {
            // should never happen
            return new Response('Not found', { status: 404 })
        }

        return routerResponse
    })())
})

self.addEventListener('message', onServiceWorkerMesssage)
